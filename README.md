## Tabla de contenidos

1. [Introducción](https://gitlab.com/elegy0101/cuestionarios---playing-lean-2/blob/master/README.md#introducción)
2. [Justificación](https://gitlab.com/elegy0101/cuestionarios---playing-lean-2/blob/master/README.md#justificación)
*  [Selección de la tecnología](https://gitlab.com/elegy0101/cuestionarios---playing-lean-2/blob/master/README.md#selección-de-la-tecnología)
*  [Instalando MongoDB Compass](https://gitlab.com/elegy0101/cuestionarios---playing-lean-2/blob/master/README.md#instalando-mongodb-compass)
*  [Instalando MongoDB](https://gitlab.com/elegy0101/cuestionarios---playing-lean-2/blob/master/README.md#instalando-mongodb)
3. [Modelado](https://gitlab.com/elegy0101/cuestionarios---playing-lean-2/blob/master/README.md#modelado)
*  [Cuestionario previo](https://gitlab.com/elegy0101/cuestionarios---playing-lean-2/blob/master/README.md#cuestionario-previo)
*  [Cuestionario posterior](https://gitlab.com/elegy0101/cuestionarios---playing-lean-2/blob/master/README.md#cuestionario-posterior)
*  [Modelo de documentos](https://gitlab.com/elegy0101/cuestionarios---playing-lean-2/blob/master/README.md#modelo-de-documentos)
4. [Implementación](https://gitlab.com/elegy0101/cuestionarios---playing-lean-2/blob/master/README.md#implementación)
*  [Conexión y creación de base de datos](https://gitlab.com/elegy0101/cuestionarios---playing-lean-2/blob/master/README.md#conexión-y-creación-de-base-de-datos)
*  [Insertando documento](https://gitlab.com/elegy0101/cuestionarios---playing-lean-2/blob/master/README.md#insertando-documento)
*  [Insertando múltiples documentos a la vez](https://gitlab.com/elegy0101/cuestionarios---playing-lean-2/blob/master/README.md#insertando-múltiples-documentos-a-la-vez)
*  [Actualizando documentos](https://gitlab.com/elegy0101/cuestionarios---playing-lean-2/blob/master/README.md#actualizando-documentos)
*  [Calculando promedio](https://gitlab.com/elegy0101/cuestionarios---playing-lean-2/blob/master/README.md#calculando-promedio)
*  [Calculando desviación estándar](https://gitlab.com/elegy0101/cuestionarios---playing-lean-2/blob/master/README.md#calculando-desviación-estándar)
*  [Analizando Schema desde MongoDB Compass](https://gitlab.com/elegy0101/cuestionarios---playing-lean-2/blob/master/README.md#analizando-schema-desde-mongodb-compass)
5. [Conclusión](https://gitlab.com/elegy0101/cuestionarios---playing-lean-2/blob/master/README.md#conclusión)
*  [Aprendizaje de materia de Bases de Datos NO SQL](https://gitlab.com/elegy0101/cuestionarios---playing-lean-2/blob/master/README.md#aprendizaje-de-materia-de-bases-de-datos-no-sql)




## Introducción 

La innovación y emprendimiento son fundamentales para el desarrollo de la economia de un país. El enfoque Lean es escencial para que
empresas y agencias gubernamentales brinden innovación a toda velocidad (Blank, 2013). La Metolodología Lean Startup se ha convertido
en una herramienta primordial para el emprendimiento. Como resultado, es un referente dentro de programas de capacitación e incluso
educativos, donde se busca impulsar la creación de nuevos productos y servicios a partir de una idea de negocio. Aunque la popularidad
de la metodología Lean Startup ha crecido durante los últimos años, hay una escases de artículos dentro de la literatura sobre como enseñar 
Lean Startup a empresas ó universidades a través de métodos de enseñanza más efectivos que permitan mejorar el resultado de aprendizaje.

El mundo está en constante cambio y así mismo la manera en que las personas aprenden. Las tecnologías de aprendizaje (LT) son una oportunidad 
para compartir y aprender conocimientos y habilidades en diferentes campos de todo el mundo. 

Cuando se habla sobre las tecnologías de aprendizaje, lo primero que se viene a la mente son los cursos en línea, como las plataformas de E-Learning o 
los tutoriales en video, sin embargo, existe un ecosistema de LT donde se puede encontrar alternativas a métodos de aprendizaje como son los juegos serios 
(Ulicsak 2010). 

"Los juegos serios son juegos diseñados en los que la educación (en sus diversas formas) es el objetivo principal, en lugar del entretenimiento 
(Gross 2016)". 

Existen múltiples campos de aplicación para los juegos serios, la educación, el ejército, la política y la atención médica o corporativa, todos con fines 
de capacitación, desarrollo de habilidades y aprendizaje o herramientas complementarias, esto no significa que los juegos serios no sean entretenidos o 
divertidos, simplemente no es el objetivo principal (Sloetjes 2014). 

En particular, hay muy poco escrito acerca del despliegue de juegos serios y Lean Startup donde se describa el proceso y resultados a detalle. Con base en está observación, 
explorar un método de enseñanza en este caso el uso de juegos serios que incrementen el nivel de apredizaje y eficiencia al momento de impartir cursos o talleres sobre la métodología 
Lean Startup resulta un tema importante, asi mísmo, medir el desempeño y presentar los resultados de la enseñanza se considera un aspecto revelante para tener un mayor conocimiento sobre 
como puede enseñarse Lean Startup de manera efectiva y al mismo tiempo conocer la efectividad de los talleres con esta experiencia de aprendizaje. Basado en lo anterior el proposito de la
presente investigación es analizar el rendimiento de aprendizaje al enseñar Lean Startup a traves de un juego serio.

Playing Lean 2 es un juego de mesa que se utiliza como herramienta educativa para enseñar los principios básicos de la Metodología Lean Startup. Por lo tanto, dicho juego se utilizará
para enseñar a varios grupos conformados por estudiantes, profesionales, empresarios y emprendedores mediante talleres. Se va a realizar una cuestionario de evaluación antes de comenzar
y otra cuestionario después de haber jugado. Esto permite comparar las respuestas de los participantes acerca de su aprendizaje sobre la Metodología Lean Startup después de haber jugado.

Como parte de la materia de Bases de Datos NOSQL, se presenta el modelado e implementación de los cuestionarios mencionados anteriormente. Así mismo, estos cuestionarios son parte
de los experimentos que se llevarán a cabo en los próximos talleres de capacitación, por lo que se presenta de que manera van a utilizarse para la recolección de datos.

## Justificación

Para recolectar datos, usualmente utilizamos un cuestionario diseñado especialmente para deteminada encuesta. Existen diferentes objetivos para la recolección
de datos, ya sea para realizar un censo poblacional, obtener feedback sobre el nivel de satisfacción de un servicio, o como en el caso del presente trabajo
obtener información acerca del antes y después de jugar Playing Lean 2 de manera que pueda medirse el rendimiento de aprendizaje de la metodología Lean Startup.

Por lo tanto, existen algunas consideraciones a tomar en cuenta:

* Cada encuesta tiene características diferentes causadas por la diferencia de características de los datos que se recopilan. Incluso en la misma encuesta, puede tener
diferentes preguntas.
* Otra diferencia es que la misma pregunta puede tener diferentes conceptos y definiciones.
* La encuesta tiene una jerarquía de datos, datos demográficos acerca del participante y sus respuestas correspondientes.
* Las respuestas de los participantes pueder ser datos numéricos y datos no numéricos.
* Una pregunta puede no aparecer nuevamente en el próximo experimento/taller. Una pregunta dentro de un cuestionario puede necesitar ser alterada de un momento para otro. 
  Estos cambios pueden generar inconsistencias, especialmente si se convierte a código la respuesta de una pregunta en un tiempo de experimentación.

El almacenamiento de datos debe permitir acomodar estos cambios históricos. Si los problemas o cambios no se solucionan, puede dar lugar a una inconsistencia de datos. 
Así para resolver estos problemas y evitar futuras complicaciones, se propone un modelado NoSQL orientado a documentos para gestionar los cambios.

Los modelos NOSQL permiten utilizar un enfoque orientado a documentos que hacen más rápida la lectura de datos, se utilizará **MongoDB** como sistema de base de datos
orientado a documentos.

Además, la flexibilidad es muy importante en este caso. En cualquier momento es posible que sea necesario agregarse o eliminarse preguntas del cuestionario y el modelo 
debería poder adaptarse con elegancia.

## Selección de la tecnología

**MongoDB** es un programa de base de datos orientado a documentos multiplataforma gratuito y de código abierto. Clasificado como un programa de base de datos NoSQL, MongoDB utiliza documentos similares a [JSON](https://en.wikipedia.org/wiki/JSON) 
con esquemas. MongoDB es desarrollado por [MongoDB Inc.](https://en.wikipedia.org/wiki/MongoDB_Inc.), y se publica bajo una combinación de la [Licencia Pública Del Lado del Servidor](https://www.mongodb.com/licensing/server-side-public-license) y la [Licencia Apache](https://en.wikipedia.org/wiki/Apache_License).

MongoDB es conocido por su alto rendimiento, alta disponibilidad, y bajo costo debido a estas cuatro propiedades:

1. **Orientado a documentos**. En lugar de tomar un tema comercial y dividirlo en múltiples estructuras relacionales, MongoDB puede almacenar el tema comercial en el mínimo número de documentos. 
   Por ejemplo, en lugar de almacenar información sobre el título y el autor en dos estructuras relacionales distintas, el título, el autor y otra información relacionada con el título se 
   pueden almacenar en un solo documento llamado Libro, que es mucho más intuitivo y generalmente más fácil de trabajar.
2. **Extremadamente extensible**. El enfoque orientado a documentos permite representar relaciones jerárquicas complejas en un solo lugar. No necesita definir esquemas con anticipación; en su lugar, 
  puede definir nuevos tipos de datos (llamados "campos") a medida que agrega los datos reales. Esto es muy diferente de construir bases de datos relacionales tradicionales donde necesita definir la estructura antes de que se complete con datos. 
  En MongoDB, puede completar y definir la estructura en al mismo tiempo. Esto hace que el desarrollo tome menos tiempo y permite que el equipo del proyecto experimente fácilmente con diferentes soluciones y luego elija la mejor.
3. **Escalabiliad horizontal**. MongoDB fue diseñado para escalar. Los documentos son
  relativamente fácil de particionar en múltiples servidores. MongoDB puede equilibrar automáticamente los datos entre servidores y redistribuir documentos, enrutando automáticamente las solicitudes de los usuarios a las máquinas correctas. 
  Cuando se necesita más capacidad, se pueden agregar nuevas máquinas y MongoDB descubrirá cómo se les deben asignar los datos existentes.
4. **Muchos controladores**. Un controlador es un traductor entre un programa y una plataforma (la plataforma en este caso es MongoDB). MongoDB tiene un conjunto oficial de controladores de funciones principales como find() y update (). Adicionalmente,
  La comunidad de desarrollo tiene muchos otros controladores para varios idiomas, como Ruby, Python y C, C++, Java, PHP, Perl, Javascript, Scala, C# / .NET, Node.js. La lista completa de controladores aparece en https://docs.mongodb.org/ecosystem/drivers/. 
  Esto permite a los desarrolladores usar su lenguaje de elección y aprovechar sus habilidades existentes para construir aplicaciones MongoDB aún más rápido y más eficientemente.

## Instalando MongoDB Compass

MongoDB Compass es la GUI para MongoDB. Compass permite analizar y comprender el contenido de sus datos sin un conocimiento formal de la sintaxis de consulta de MongoDB. Además de explorar sus datos en un entorno visual, también puede usar Compass para optimizar 
el rendimiento de las consultas, administrar índices e implementar la validación de documentos.

Se pueden consultar las siguientes guías según el sistema operativo a utilizar:

* [Documentación oficial de MongoDB](https://docs.mongodb.com/compass/master/install/)

## Instalando Mongo Shell

Compass no es totalmente compatible con el lenguaje de consulta MongoDB. Por lo tanto, para realizar las consultas en MongoDB, vamos a necesitar instalar la shell de mongo.

La shell de mongo es un cliente basado en texto que utilizaremos mediante cualquier interfaz de línea de comandos disponible en nuestro sistema.

Se pueden consultar las siguientes guías según el sistema operativo a utilizar:

* [Documentación oficial de MongoDB](https://docs.mongodb.com/manual/installation/#mongodb-enterprise-edition-installation-tutorials)

>  Para este caso vamos a utilizar la edición empresarial de mongodb para acceder a la base de datos desde la nube.

## Modelado

Para comenzar, vale la pena hacerse algunas preguntas estratégicas. Esto ayuda a definir el alcance del modelo final.

1. ¿Para que se va a utilizar el modelo?

Este modelo va a permitir almacenar las respuestas de los cuestionarios utilizados en los talleres de Playing Lean 2 que se estarán llevando a cabo como parte de la fase de experimentación de la tesis. 
Además, proporciona la base para realizar una análisis sobre las respuestas(esto puede incluirse en un trabajo a futuro, por el momento se realizará en una herramienta de análisis de datos).

2. ¿Hay modelo base o se requiere un nuevo?

Es este caso se necesita una solución desde 0.

3. ¿Aplicar analytics es un requierimiento?

Si, será interesante realizar operaciones sobre los datos demográficos como edad, ocupación o nivel de esutudios, así como obtener el promedio o desviación estándar de algún concepto en particular.

4. ¿Quién es la audiencia?

El Profesor de la materia de Bases de Datos NOSQL es el principal usuario que validará el modelo y al ser parte del experimento también está interesado en revisar
los resultados de los cuestionarios, así mismo, el tesista y asesor de tesis son dos usuarios altamente interesados.

5. ¿Flexibilidad o usabilidad?

La flexibilidad es muy importante en este caso. En cualquier momento es posible que sea necesario agregarse o eliminarse preguntas del cuestionario y el modelo 
debería poder adaptarse con elegancia.

## Cuestionario previo

**Objetivo de la encuesta:** Recolectar el conocimiento base antes de jugar Playing Lean 2

Esta encuesta se realiza en conjunto con una tesis de Maestría en Ingeniería de Software dentro de CIMAT - Zacatecas, investigando el resultado del aprendizaje utilizando un juego serio. 
Todas las respuestas son anónimas y tomará aproximadamente 10 minutos completarlas. Apreciamos que se esté tomando el tiempo para completar la encuesta.

A continuación de definen las preguntas que contendrá el cuestionario con sus posibles respuestas:

1. **¿Género?**

*  Hombre
*  Mujer

2. **¿Edad?**

3. **¿Cuál es su nivel de estudios?**


*  Preparatoria
*  Carrera trunca
*  Carrera técnica
*  Licenciatura
*  Maestría
*  Doctorado
*  Ninguno

4. **¿Ocupación?**

* Emprendedor
* Estudiante
* Empleado
* Independiente
* Jubilado
* Desempleado en busca de trabajo
* Sin disponibilidad para trabajar

5. **¿Conocimiento sobre Lean Startup?**

*  Ninguno
*  Poco
*  Medio
*  Alto

6. **En caso de haber tenido conocimiento previo, seleccione de que manera lo obtuvo:**

*  Curso tradicional
*  Curso utilizando un juego serio
*  Leí el libro
*  Artículo de Internet
*  Un amigo o conocido me hablo del tema
*  Otro

A continuación se describen varios elementos de la metodología Lean Startup. 

**¿Qué tan seguro está de su comprensión?**

Por favor, califique su grado de compresión registrando un número del 0 al 10 utilizando la siguiente escala:

![ALT](/img/escala.png)

**7. La Metodología Lean Startup en general:**

R:

**8. Circuito de Feedback (Construir-medir,aprender):**

R:

**9. Pivotar**

R:

**10. Salir del edificio:**

R:

**11. Propuesta de valor canvas:**

R:

**12. Iteraciones rápidas:**

R:

**13. Producto Mínimo Víable (MVP):**

R:

**14. Contabilidad de la innovación:**

R:

**15. Perfil del cliente:**

R:

**16. Ajuste Problema-Solución:**

R:

**17. Ajuste Producto-Mercado:**

R:

**18. Scalabilidad y sincronización**

R:

## Cuestionario posterior

**Objetivo de la encuesta:** Medir la mejora de aprendizaje despúes de jugar Playing Lean 2

A continuación de definen las preguntas que contendrá el cuestionario con sus posibles respuestas:

Elementos de la metodología Lean Startup. 

**¿Qué tan seguro está de su comprensión?**

Por favor, califique su grado de compresión registrando un número del 0 al 10 utilizando la siguiente escala:

![ALT](/img/escala.png)

**1. La Metodología Lean Startup en general:**

R:

**2. Circuito de Feedback (Construir-medir,aprender):**

R:

**3. Pivotar**

R:

**4. Salir del edificio:**

R:

**5. Propuesta de valor canvas:**

R:

**6. Iteraciones rápidas:**

R:

**7. Producto Mínimo Víable (MVP):**

R:

**8. Contabilidad de la innovación:**

R:

**9. Perfil del cliente:**

R:

**10. Ajuste Problema-Solución:**

R:

**11. Ajuste Producto-Mercado:**

R:

**12. Scalabilidad y sincronización**

R:

¿Cuál de las siguientes mecánicas de juego te ha ayudado a comprender mejor los principios fundamentales de la metodología Lean Startup? 
(seleccione todas las que correspondan):

**1. La Metodología Lean Startup en general:**

* Cartas de experimentos 
* Tablero de clientes
* Hoja de innovación contable
* Instructivo del juego
* Cartas de compañias
* Interactuar con otros jugadores
* Ayuda del facilitador
* Ninguno de los anteriores

**2. Circuito de Feedback (Construir-medir,aprender):**

* Cartas de experimentos 
* Tablero de clientes
* Hoja de innovación contable
* Instructivo del juego
* Cartas de compañias
* Interactuar con otros jugadores
* Ayuda del facilitador
* Ninguno de los anteriores


**3. Pivotar**

* Cartas de experimentos 
* Tablero de clientes
* Hoja de innovación contable
* Instructivo del juego
* Cartas de compañias
* Interactuar con otros jugadores
* Ayuda del facilitador
* Ninguno de los anteriores

**4. Salir del edificio:**

* Cartas de experimentos 
* Tablero de clientes
* Hoja de innovación contable
* Instructivo del juego
* Cartas de compañias
* Interactuar con otros jugadores
* Ayuda del facilitador
* Ninguno de los anteriores


**5. Propuesta de valor canvas:**

* Cartas de experimentos 
* Tablero de clientes
* Hoja de innovación contable
* Instructivo del juego
* Cartas de compañias
* Interactuar con otros jugadores
* Ayuda del facilitador
* Ninguno de los anteriores


**6. Iteraciones rápidas:**

* Cartas de experimentos 
* Tablero de clientes
* Hoja de innovación contable
* Instructivo del juego
* Cartas de compañias
* Interactuar con otros jugadores
* Ayuda del facilitador
* Ninguno de los anteriores


**7. Producto Mínimo Víable (MVP):**

* Cartas de experimentos 
* Tablero de clientes
* Hoja de innovación contable
* Instructivo del juego
* Cartas de compañias
* Interactuar con otros jugadores
* Ayuda del facilitador
* Ninguno de los anteriores


**8. Contabilidad de la innovación:**

* Cartas de experimentos 
* Tablero de clientes
* Hoja de innovación contable
* Instructivo del juego
* Cartas de compañias
* Interactuar con otros jugadores
* Ayuda del facilitador
* Ninguno de los anteriores


**9. Perfil del cliente:**

* Cartas de experimentos 
* Tablero de clientes
* Hoja de innovación contable
* Instructivo del juego
* Cartas de compañias
* Interactuar con otros jugadores
* Ayuda del facilitador
* Ninguno de los anteriores


**10. Ajuste Problema-Solución:**

* Cartas de experimentos 
* Tablero de clientes
* Hoja de innovación contable
* Instructivo del juego
* Cartas de compañias
* Interactuar con otros jugadores
* Ayuda del facilitador
* Ninguno de los anteriores


**11. Ajuste Producto-Mercado:**

* Cartas de experimentos 
* Tablero de clientes
* Hoja de innovación contable
* Instructivo del juego
* Cartas de compañias
* Interactuar con otros jugadores
* Ayuda del facilitador
* Ninguno de los anteriores

**12. Scalabilidad y sincronización**

* Cartas de experimentos 
* Tablero de clientes
* Hoja de innovación contable
* Instructivo del juego
* Cartas de compañias
* Interactuar con otros jugadores
* Ayuda del facilitador
* Ninguno de los anteriores


## Modelo de Documentos 

El objetivo principal del experimento es explorar las preguntas y respuestas de los cuestionarios de los talleres de capacitación, por lo tanto, la solución
es una única colección de documentos que representan la pregunta y valor correspondiente. Tratar los cuestionarios y las preguntas como documentos brinda
mayor flexibilidad para representar la dependencia de cada pregunta y su respuesta correspondiente en lugar de tener varios documentos relacionados entre sí.

Representación de modelo de encuestas:

![ALT](/img/modelo-encuestas.png)

Otro enfoque puede ser utilizar subdocumentos para cada set de preguntas, sin embargo, se tomo está decisión de modelado con base a la naturalidad de las encuestas.
Una encuesta es un documento como tal con preguntas de manera consecutiva donde pueda distinguirse la pregunta y su posible respuesta por lo que el modelo luce
como se vería una encuesta en papel. Puede considerarse anidar las preguntas y respuestas en subdocumentos si se requiere mayor rendimiento al momento de realizar 
consultas pero por ahora lo más importante es la flexibilidad y legibilidad dentro de la colección.

A continuación se muestra como quedaría el primer documento correspondiente a la encuesta de evaluación previa:

Encuesta previa:

```json

{
  "_id": "5dc21397c91ace3f5fe71918",
    "tipo": "antes",
    "fecha_aplicación": "2019-10-29",
    "genéro": "Hombre",
    "edad": 44,
    "nivel_estudios": "Doctorado",
    "ocupacion": "Empleado",
    "conocimiento_inicial": "Experiencia media",
    "conocimiento_inicial": ["Curso utilizando algún juego serio", "Lei el libro"],
    "lean_startup_general": 8,
    "circuito_feedback": 8,
    "pivotar": 8,
    "propuesta_valor_canvas": 8,
    "iteraciones_rapidas": 5,
    "producto_minimo_viable": 8,
    "contabilidad_innovacion": 5,
    "perfil_cliente": 8,
    "ajuste_problema_solucion": 8
}

```

Enseguida el documento para la encuesta posterior:

```json

{
      "_id": "5dc58eb99d0652c41b01e3a0",
      "tipo": "despues",
      "fecha_aplicación": "2019-10-29",
      "genéro": "Mujer",
      "edad": 40,
      "nivel_estudios": "Licenciatura",
      "ocupacion": "Independiente",
      "lean_startup_general": [{
        "respuesta": 9,
        "elementos": ["Cartas de experimentos", "Tablero de clientes", "Tablero inicial de Ingenieria"]
      }],
      "circuito_feedback": [{
        "respuesta": 9,
        "elementos": ["Cartas de experimentos", "Tablero inicial de Ingenieria"]
      }],
      "pivotar": [{
        "respuesta": 9,
        "elementos": ["Instrucciones del facilitador", "Cartas de experimentos"]
      }],
      "propuesta_valor_canvas": [{
        "respuesta": 8,
        "elementos": ["Cartas de experimentos"]
      }],
      "iteraciones_rapidas": [{
        "respuesta": 8,
        "elementos": ["Product backlog", "Pila de características de producto", "Bloque inicial de experimentos"]
      }],
      "producto_minimo_viable": [{
        "respuesta": 8,
        "elementos": ["Cartas de experimentos", "Product backlog", "Pila de características de producto", "Bloque inicial de experimentos"]
      }],
      "contabilidad_innovacion": [{
        "respuesta": 8,
        "elementos": ["Cartas de experimentos", "Hoja de contabilidad de la innovacion"]
      }],
      "perfil_cliente": [{
        "respuesta": 8,
        "elementos": ["Cartas de experimentos"]
      }],
      "ajuste_problema_solucion": [{
        "respuesta": 9,
        "elementos": ["Cartas de experimentos"]
      }]
    }

```

>  Para el documento de encuestas posteriores se utiliza un arreglo que contiene un documento con la respuesta y los elementos/mecánicas de juego
que el encuestado seleccionó.

## Implementación

## Conexión y creación de base de datos

Vamos a conectarnos a un cluster de la nube de MongoDB Atlas. La conexión también puede hacerse de manera local, sin embargo, se decidió utilizar un cluster
para jugar un poco y acceder a la base de datos desde cualquier lugar, ya sea desde Compass ó a través de la shell de mongo.

>  Para más información sobre sobre el servicio en la nube de mongo visitar el sitio oficial de [MongoDB Atlas](https://www.mongodb.com/cloud/atlas)

![ALT](/img/conexion.png)

Una vez conectados, creamos una base de datos con el nombre de "encuestas" que a su vez tendrá una collección llamada "resultados":

![ALT](/img/creando-db.png)

Ya tenemos lista nuestra base de datos para comenzar a insertar documentos.

## Insertando documento

La mayoría de las consultas se van a realizar usando la shell de mongo por lo que ahora vamos a conectarnos al cluster usando la terminal del sistema.

Accedemos con el siguiente comando:

`mongo "mongodb+srv://cluster0-hy0xg.mongodb.net/test"  --username mongo-user --password my-password`

![ALT](/img/terminal.png)

Si la conexión fue existosa veremos el prompt con algo similar a lo siguiente:

![ALT](/img/prompt.png)

El query para insertar nuestro primer documento quedaría como se muestra a continuación:

```sql

db.resultados.insertOne({
    "tipo": "antes",
    "fecha_aplicación": "2019-10-29",
    "genéro": "Hombre",
    "edad": 44,
    "nivel_estudios": "Doctorado",
    "ocupacion": "Empleado",
    "conocimiento_inicial": "Experiencia media",
    "conocimiento_inicial": ["Curso utilizando algún juego serio", "Lei el libro"],
    "lean_startup_general": 8,
    "circuito_feedback": 8,
    "pivotar": 8,
    "propuesta_valor_canvas": 8,
    "iteraciones_rapidas": 5,
    "producto_minimo_viable": 8,
    "contabilidad_innovacion": 5,
    "perfil_cliente": 8,
    "ajuste_problema_solucion": 8
  })

```

## Insertando múltiples documentos a la vez

Para insertar múltiples documentos en una misma consulta se utiliza el método `insertMany()`.

**Encuesta previa:**

```sql
db.resultados.insertMany(
  [{
    "tipo": "antes",
    "fecha_aplicación": "2019-10-29",
    "genéro": "Mujer",
    "edad": 27,
    "nivel_estudios": "Licenciatura",
    "ocupacion": "Emprendedor",
    "conocimiento_inicial": "Experiencia media",
    "obtencion_conocimiento": ["Curso tradicional", "Leí el libro", "Artículo de internet", "Un amigo me hablo del tema"],
    "lean_startup_general": 9,
    "circuito_feedback": 10,
    "pivotar": 10,
    "propuesta_valor_canvas": 9,
    "iteraciones_rapidas": 9,
    "producto_minimo_viable": 9,
    "contabilidad_innovacion": 7,
    "perfil_cliente": 9,
    "ajuste_problema_solucion": 9
  }, {
    "tipo": "antes",
    "fecha_aplicación": "2019-10-29",
    "genéro": "Hombre",
    "edad": 40,
    "nivel_estudios": "Licenciatura",
    "ocupacion": "Desempleado en busca de trabajo",
    "conocimiento_inicial": "Nada de experiencia",
    "lean_startup_general": 0,
    "circuito_feedback": 0,
    "pivotar": 0,
    "propuesta_valor_canvas": 0,
    "iteraciones_rapidas": 0,
    "producto_minimo_viable": 0,
    "contabilidad_innovacion": 0,
    "perfil_cliente": 0,
    "ajuste_problema_solucion": 0
  }, {
    "tipo": "antes",
    "fecha_aplicación": "2019-10-29",
    "genéro": "Mujer",
    "edad": 40,
    "nivel_estudios": "Maestria",
    "ocupacion": "Independient",
    "conocimiento_inicial": "Mucha experiencia",
    "obtencion_conocimiento": ["Curso tradicional", "Curso utilizando algún juego serio", "Leí el libro", "Articulo de Internet", "Un amigo o conocido me hablo del tema"],
    "lean_startup_general": 10,
    "circuito_feedback": 10,
    "pivotar": 10,
    "propuesta_valor_canvas": 10,
    "iteraciones_rapidas": 10,
    "producto_minimo_viable": 10,
    "contabilidad_innovacion": 10,
    "perfil_cliente": 10,
    "ajuste_problema_solucion": 10
  }, {
    "tipo": "antes",
    "fecha_aplicación": "2019-10-29",
    "genéro": "Hombre",
    "edad": 43,
    "nivel_estudios": "Maestria",
    "ocupacion": "Emprendedor",
    "conocimiento_inicial": "Poca experiencia",
    "obtencion_conocimiento": ["Curso utilizando algun juego serio", "Leí el libro", "Curso Steve Blank - Audacity"],
    "lean_startup_general": 5,
    "circuito_feedback": 7,
    "pivotar": 7,
    "propuesta_valor_canvas": 7,
    "iteraciones_rapidas": 8,
    "producto_minimo_viable": 6,
    "contabilidad_innovacion": 5,
    "perfil_cliente": 5,
    "ajuste_problema_solucion": 6
  }, {
    "tipo": "antes",
    "fecha_aplicación": "2019-10-29",
    "genéro": "Hombre",
    "edad": 45,
    "nivel_estudios": "Maestria",
    "ocupacion": "Empleado",
    "conocimiento_inicial": "Nada de experiencia",
    "lean_startup_general": 0,
    "circuito_feedback": 0,
    "pivotar": 0,
    "propuesta_valor_canvas": 0,
    "iteraciones_rapidas": 0,
    "producto_minimo_viable": 0,
    "contabilidad_innovacion": 0,
    "perfil_cliente": 0,
    "ajuste_problema_solucion": 0
  }, {
    "tipo": "antes",
    "fecha_aplicación": "2019-10-29",
    "genéro": "Hombre",
    "edad": 44,
    "nivel_estudios": "Maestria",
    "ocupacion": "Empleado",
    "conocimiento_inicial": "Experiencia media",
    "obtencion_conocimiento": "Articulo de Internet",
    "lean_startup_general": 6,
    "circuito_feedback": 6,
    "pivotar": 8,
    "propuesta_valor_canvas": 8,
    "iteraciones_rapidas": 8,
    "producto_minimo_viable": 8,
    "contabilidad_innovacion": 4,
    "perfil_cliente": 9,
    "ajuste_problema_solucion": 7
  }, {
    "tipo": "antes",
    "fecha_aplicación": "2019-10-29",
    "genéro": "Mujer",
    "edad": 40,
    "nivel_estudios": "Licenciatura",
    "ocupacion": "Independiente",
    "conocimiento_inicial": "Poca experiencia",
    "obtencion_conocimiento": "Curso tradicional",
    "lean_startup_general": 9,
    "circuito_feedback": 8,
    "pivotar": 8,
    "propuesta_valor_canvas": 8,
    "iteraciones_rapidas": 8,
    "producto_minimo_viable": 8,
    "contabilidad_innovacion": 4,
    "perfil_cliente": 9,
    "ajuste_problema_solucion": 9
  }, {
    "tipo": "antes",
    "fecha_aplicación": "2019-10-29",
    "genéro": "Mujer",
    "edad": 42,
    "nivel_estudios": "Maestria",
    "ocupacion": "Empleado",
    "conocimiento_inicial": "Mucha experiencia",
    "obtencion_conocimiento": "Curso tradicional",
    "lean_startup_general": 10,
    "circuito_feedback": 9,
    "pivotar": 10,
    "propuesta_valor_canvas": 10,
    "iteraciones_rapidas": 10,
    "producto_minimo_viable": 10,
    "contabilidad_innovacion": 8,
    "perfil_cliente": 10,
    "ajuste_problema_solucion": 10
  }, {
    "tipo": "antes",
    "fecha_aplicación": "2019-10-29",
    "genéro": "Hombre",
    "edad": 28,
    "nivel_estudios": "Licenciatura",
    "ocupacion": "Independiente",
    "conocimiento_inicial": "Mucha experiencia",
    "obtencion_conocimiento": ["Curso tradicional", "Curso utilizando algun juego serio"],
    "lean_startup_general": 10,
    "circuito_feedback": 10,
    "pivotar": 10,
    "propuesta_valor_canvas": 10,
    "iteraciones_rapidas": 10,
    "producto_minimo_viable": 9,
    "contabilidad_innovacion": 9,
    "perfil_cliente": 10,
    "ajuste_problema_solucion": 10
  }, {
    "tipo": "antes",
    "fecha_aplicación": "2019-10-29",
    "genéro": "Mujer",
    "edad": 35,
    "nivel_estudios": "Maestria",
    "ocupacion": "Empleado",
    "conocimiento_inicial": "Experiencia media",
    "obtencion_conocimiento": ["Curso tradicional", "Un amigo o conocido me hablo del tema"],
    "lean_startup_general": 7,
    "circuito_feedback": 7,
    "pivotar": 9,
    "propuesta_valor_canvas": 8,
    "iteraciones_rapidas": 7,
    "producto_minimo_viable": 9,
    "contabilidad_innovacion": 6,
    "perfil_cliente": 9,
    "ajuste_problema_solucion": 7
  }, {
    "tipo": "antes",
    "fecha_aplicación": "2019-10-29",
    "genéro": "Hombre",
    "edad": 38,
    "nivel_estudios": "Maestria",
    "ocupacion": "Emprendedor",
    "conocimiento_inicial": "Experiencia media",
    "obtencion_conocimiento": "Participacion en el ecosistema de emprendimiento",
    "lean_startup_general": 9,
    "circuito_feedback": 9,
    "pivotar": 10,
    "propuesta_valor_canvas": 10,
    "iteraciones_rapidas": 9,
    "producto_minimo_viable": 10,
    "contabilidad_innovacion": 9,
    "perfil_cliente": 10,
    "ajuste_problema_solucion": 9
  }, {
    "tipo": "antes",
    "fecha_aplicación": "2019-10-29",
    "genéro": "Mujer",
    "edad": 38,
    "nivel_estudios": "Maestria",
    "ocupacion": "Emprendedor",
    "conocimiento_inicial": "Poca experiencia",
    "lean_startup_general": 2,
    "circuito_feedback": 1,
    "pivotar": 0,
    "propuesta_valor_canvas": 6,
    "iteraciones_rapidas": 1,
    "producto_minimo_viable": 1,
    "contabilidad_innovacion": 1,
    "perfil_cliente": 6,
    "ajuste_problema_solucion": 7
  }]
)

 ```
 
 **Encuesta posterior:**
 
 ```sql

db.resultados.insertMany(

    [{
      "tipo": "despues",
      "fecha_aplicación": "2019-10-29",
      "genéro": "Mujer",
      "edad": 40,
      "nivel_estudios": "Licenciatura",
      "ocupacion": "Independiente",
      "lean_startup_general": [{
        "respuesta": 9,
        "elementos": ["Cartas de experimentos", "Tablero de clientes", "Tablero inicial de Ingenieria"]
      }],
      "circuito_feedback": [{
        "respuesta": 9,
        "elementos": ["Cartas de experimentos", "Tablero inicial de Ingenieria"]
      }],
      "pivotar": [{
        "respuesta": 9,
        "elementos": ["Instrucciones del facilitador", "Cartas de experimentos"]
      }],
      "propuesta_valor_canvas": [{
        "respuesta": 8,
        "elementos": ["Cartas de experimentos"]
      }],
      "iteraciones_rapidas": [{
        "respuesta": 8,
        "elementos": ["Product backlog", "Pila de características de producto", "Bloque inicial de experimentos"]
      }],
      "producto_minimo_viable": [{
        "respuesta": 8,
        "elementos": ["Cartas de experimentos", "Product backlog", "Pila de características de producto", "Bloque inicial de experimentos"]
      }],
      "contabilidad_innovacion": [{
        "respuesta": 8,
        "elementos": ["Cartas de experimentos", "Hoja de contabilidad de la innovacion"]
      }],
      "perfil_cliente": [{
        "respuesta": 8,
        "elementos": ["Cartas de experimentos"]
      }],
      "ajuste_problema_solucion": [{
        "respuesta": 9,
        "elementos": ["Cartas de experimentos"]
      }],
    }, {
      "tipo": "despues",
      "fecha_aplicación": "2019-10-29",
      "genéro": "Hombre",
      "edad": 37,
      "nivel_estudios": "Licenciatura",
      "ocupacion": "Empleado",
      "lean_startup_general": [{
        "respuesta": 9,
        "elementos": ["Tablero de clientes", "Tablero inicial de Ingenieria"]
      }],
      "circuito_feedback": [{
        "respuesta": 9,
        "elementos": ["Cartas de experimentos", "Tablero inicial de Ingenieria"]
      }],
      "pivotar": [{
        "respuesta": 10,
        "elementos": ["Cartas de experimentos"]
      }],
      "propuesta_valor_canvas": [{
        "respuesta": 10,
        "elementos": ["Cartas de experimentos"]
      }],
      "iteraciones_rapidas": [{
        "respuesta": 10,
        "elementos": ["Tablero inicial de Ingenieria", "Pila de características de producto"]
      }],
      "producto_minimo_viable": [{
        "respuesta": 10,
        "elementos": ["Tablero inicial de Ingenieria", "Bloque inicial de experimentos"]
      }],
      "contabilidad_innovacion": [{
        "respuesta": 9,
        "elementos": ["Hoja de contabilidad de la innovacion", "Instrucciones del facilitador"]
      }],
      "perfil_cliente": [{
        "respuesta": 9,
        "elementos": ["Cartas de experimentos", "Bloque inicial de experimentos"]
      }],
      "ajuste_problema_solucion": [{
        "respuesta": 9,
        "elementos": ["Cartas de experimentos", "Tablero de clientes"]
      }],
    }, {
      "tipo": "despues",
      "fecha_aplicación": "2019-10-29",
      "genéro": "Hombre",
      "edad": 40,
      "nivel_estudios": "Maestria",
      "ocupacion": "Desempleado en busca de trabajo",
      "lean_startup_general": [{
        "respuesta": 10,
        "elementos": ["Cartas de experimentos", "Tablero de clientes"]
      }],
      "circuito_feedback": [{
        "respuesta": 8,
        "elementos": ["Cartas de experimentos", "Tablero inicial de Ingenieria", "Product backlog"]
      }],
      "pivotar": [{
        "respuesta": 10,
        "elementos": ["Cartas de experimentos", "Instrucciones del facilitador"]
      }],
      "propuesta_valor_canvas": [{
        "respuesta": 6,
        "elementos": ["Cartas de experimentos", "Instrucciones del facilitador"]
      }],
      "iteraciones_rapidas": [{
        "respuesta": 8,
        "elementos": ["Ninguna de las anteriores"]
      }],
      "producto_minimo_viable": [{
        "respuesta": 9,
        "elementos": ["Pila de características de producto"]
      }],
      "contabilidad_innovacion": [{
        "respuesta": 9,
        "elementos": ["Hoja de contabilidad de la innovacion"]
      }],
      "perfil_cliente": [{
        "respuesta": 10,
        "elementos": ["Cartas de experimentos"]
      }],
      "ajuste_problema_solucion": [{
        "respuesta": 9,
        "elementos": ["Instrucciones del facilitador"]
      }]
    }, {
      "tipo": "despues",
      "fecha_aplicación": "2019-10-29",
      "genéro": "Hombre",
      "edad": 26,
      "nivel_estudios": "Licenciatura",
      "ocupacion": "Independiente",
      "lean_startup_general": [{
        "respuesta": 10,
        "elementos": ["Tablero de clientes", "Tablero inicial de Ingenieria"]
      }],
      "circuito_feedback": [{
        "respuesta": 10,
        "elementos": ["Tablero inicial de Ingenieria"]
      }],
      "pivotar": [{
        "respuesta": 10,
        "elementos": ["Cartas de experimentos"]
      }],
      "propuesta_valor_canvas": [{
        "respuesta": 10,
        "elementos": ["Cartas de experimentos", "Instrucciones del facilitador"]
      }],
      "iteraciones_rapidas": [{
        "respuesta": 8,
        "elementos": ["Ninguna de las anteriores"]
      }],
      "producto_minimo_viable": [{
        "respuesta": 10,
        "elementos": ["Product backlog", "Pila de características de producto"]
      }],
      "contabilidad_innovacion": [{
        "respuesta": 8,
        "elementos": ["Hoja de contabilidad de la innovacion"]
      }],
      "perfil_cliente": [{
        "respuesta": 10,
        "elementos": ["Instrucciones del facilitador"]
      }],
      "ajuste_problema_solucion": [{
        "respuesta": 9,
        "elementos": ["Cartas de experimentos", "Tablero de clientes"]
      }]
    }, {
      "tipo": "despues",
      "fecha_aplicación": "2019-10-29",
      "genéro": "Hombre",
      "edad": 37,
      "nivel_estudios": "Maestria",
      "ocupacion": "Empleado",
      "lean_startup_general": [{
        "respuesta": 9,
        "elementos": ["Tablero de clientes", "Tablero inicial de Ingenieria"]
      }],
      "circuito_feedback": [{
        "respuesta": 9,
        "elementos": ["Manual de juego", "Tablero de clientes"]
      }],
      "pivotar": [{
        "respuesta": 10,
        "elementos": ["Cartas de experimentos"]
      }],
      "propuesta_valor_canvas": [{
        "respuesta": 8,
        "elementos": ["Manual de juego"]
      }],
      "iteraciones_rapidas": [{
        "respuesta": 8,
        "elementos": ["Ninguna de las anteriores"]
      }],
      "producto_minimo_viable": [{
        "respuesta": 9,
        "elementos": ["Tablero inicial de Ingenieria"]
      }],
      "contabilidad_innovacion": [{
        "respuesta": 7,
        "elementos": ["Hoja de contabilidad de la innovacion"]
      }],
      "perfil_cliente": [{
        "respuesta": 8,
        "elementos": ["Tablero del clientes"]
      }],
      "ajuste_problema_solucion": [{
        "respuesta": 8,
        "elementos": ["Tablero de clientes"]
      }]
    }, {
      "tipo": "despues",
      "fecha_aplicación": "2019-10-29",
      "genéro": "Hombre",
      "edad": 28,
      "nivel_estudios": "Licenciatura",
      "ocupacion": "Independiente",
      "lean_startup_general": [{
        "respuesta": 10,
        "elementos": ["Tablero de clientes", "Tablero inicial de Ingenieria"]
      }],
      "circuito_feedback": [{
        "respuesta": 10,
        "elementos": ["Cartas de experimentos"]
      }],
      "pivotar": [{
        "respuesta": 10,
        "elementos": ["Cartas de experimentos"]
      }],
      "propuesta_valor_canvas": [{
        "respuesta": 10,
        "elementos": ["Instrucciones del facilitador"]
      }],
      "iteraciones_rapidas": [{
        "respuesta": 10,
        "elementos": ["Ninguna de las anteriores"]
      }],
      "producto_minimo_viable": [{
        "respuesta": 10,
        "elementos": ["Bloque inicial de experimentos", "Tablero inicial del Ingenieria"]
      }],
      "contabilidad_innovacion": [{
        "respuesta": 9,
        "elementos": ["Cartas de experimentos", "Hoja de contabilidad de la innovacion"]
      }],
      "perfil_cliente": [{
        "respuesta": 10,
        "elementos": ["Tablero de clientes"]
      }],
      "ajuste_problema_solucion": [{
        "respuesta": 10,
        "elementos": ["Tablero del clientes"]
      }]
    }, {
      "tipo": "Mujer",
      "fecha_aplicación": "2019-10-29",
      "genéro": "Mujer",
      "edad": 42,
      "nivel_estudios": "Maestria",
      "ocupacion": "Empleado",
      "lean_startup_general": [{
        "respuesta": 10,
        "elementos": ["Cartas de experimentos", "Tablero inicial de Ingenieria"]
      }],
      "circuito_feedback": [{
        "respuesta": 10,
        "elementos": ["Instrucciones del facilitador", "Tablero inicial de Ingenieria"]
      }],
      "pivotar": [{
        "respuesta": 10,
        "elementos": ["Instrucciones del facilitador"]
      }],
      "propuesta_valor_canvas": [{
        "respuesta": 10,
        "elementos": ["Ninguna de las anteriores"]
      }],
      "iteraciones_rapidas": [{
        "respuesta": 10,
        "elementos": ["Ninguna de las anteriores"]
      }],
      "producto_minimo_viable": [{
        "respuesta": 10,
        "elementos": ["Product backlog", "Pila de características de producto", "Bloque inicial de experimentos"]
      }],
      "contabilidad_innovacion": [{
        "respuesta": 10,
        "elementos": ["Cartas de experimentos", "Hoja de contabilidad de la innovacion"]
      }],
      "perfil_cliente": [{
        "respuesta": 10,
        "elementos": ["Ninguna de las anteriores"]
      }],
      "ajuste_problema_solucion": [{
        "respuesta": 9,
        "elementos": ["Ninguna de las anteriores"]
      }]
    }, {
      "tipo": "despues",
      "fecha_aplicación": "2019-10-29",
      "genéro": "Hombre",
      "edad": 44,
      "nivel_estudios": "Doctorado",
      "ocupacion": "Empleado",
      "lean_startup_general": [{
        "respuesta": 9,
        "elementos": ["Cartas de experimentos", "Tablero inicial de Ingenieria"]
      }],
      "circuito_feedback": [{
        "respuesta": 9,
        "elementos": ["Tablero inicial de Ingenieria"]
      }],
      "pivotar": [{
        "respuesta": 9,
        "elementos": ["Instrucciones del facilitador", "Cartas de experimentos"]
      }],
      "propuesta_valor_canvas": [{
        "respuesta": 9,
        "elementos": ["Cartas de experimentos"]
      }],
      "iteraciones_rapidas": [{
        "respuesta": 9,
        "elementos": ["Ninguna de las anteriores"]
      }],
      "producto_minimo_viable": [{
        "respuesta": 9,
        "elementos": ["Cartas de experimentos", "Product backlog", "Pila de características de producto"]
      }],
      "contabilidad_innovacion": [{
        "respuesta": 8,
        "elementos": ["Ninguna de las anteriores"]
      }],
      "perfil_cliente": [{
        "respuesta": 9,
        "elementos": ["Ninguna de las anteriores"]
      }],
      "ajuste_problema_solucion": [{
        "respuesta": 9,
        "elementos": ["Tablero de clientes"]
      }]
    }, {
      "tipo": "despues",
      "fecha_aplicación": "2019-10-29",
      "genéro": "Hombre",
      "edad": 38,
      "nivel_estudios": "Maestria",
      "ocupacion": "Empleado",
      "lean_startup_general": [{
        "respuesta": 10,
        "elementos": ["Cartas de experimentos", "Tablero de clientes", "Tablero inicial de Ingenieria"]
      }],
      "circuito_feedback": [{
        "respuesta": 10,
        "elementos": ["Cartas de experimentos", "Tablero inicial de Ingenieria"]
      }],
      "pivotar": [{
        "respuesta": 10,
        "elementos": ["Instrucciones del facilitador", "Cartas de experimentos"]
      }],
      "propuesta_valor_canvas": [{
        "respuesta": 10,
        "elementos": ["Ninguno de los anteriores"]
      }],
      "iteraciones_rapidas": [{
        "respuesta": 10,
        "elementos": ["Ninguno de los anteriores"]
      }],
      "producto_minimo_viable": [{
        "respuesta": 10,
        "elementos": ["Cartas de experimentos", "Bloque inicial de experimentos"]
      }],
      "contabilidad_innovacion": [{
        "respuesta": 10,
        "elementos": ["Hoja de contabilidad de la innovacion"]
      }],
      "perfil_cliente": [{
        "respuesta": 10,
        "elementos": ["Ninguno de los anteriores"]
      }],
      "ajuste_problema_solucion": [{
        "respuesta": 10,
        "elementos": ["Ninguno de los anteriores"]
      }]
    }, {
      "tipo": "despues",
      "fecha_aplicación": "2019-10-29",
      "genéro": "Hombre",
      "edad": 43,
      "nivel_estudios": "Maestria",
      "ocupacion": "Independiente",
      "lean_startup_general": [{
        "respuesta": 6,
        "elementos": ["Cartas de experimentos"]
      }],
      "circuito_feedback": [{
        "respuesta": 8,
        "elementos": ["Cartas de experimentos", "Tablero inicial de Ingenieria"]
      }],
      "pivotar": [{
        "respuesta": 7,
        "elementos": ["Cartas de experimentos"]
      }],
      "propuesta_valor_canvas": [{
        "respuesta": 6,
        "elementos": ["Ninguno de los anteriores"]
      }],
      "iteraciones_rapidas": [{
        "respuesta": 6,
        "elementos": ["Ninguno de los anteriores"]
      }],
      "producto_minimo_viable": [{
        "respuesta": 5,
        "elementos": ["Pila de características de producto", "Bloque inicial de experimentos"]
      }],
      "contabilidad_innovacion": [{
        "respuesta": 6,
        "elementos": ["Cartas de experimentos", "Hoja de contabilidad de la innovacion"]
      }],
      "perfil_cliente": [{
        "respuesta": 6,
        "elementos": ["Ninguno de los anteriores"]
      }],
      "ajuste_problema_solucion": [{
        "respuesta": 9,
        "elementos": ["Ninguno de los anteriores"]
      }],
    }, {
      "tipo": "despues",
      "fecha_aplicación": "2019-10-29",
      "genéro": "Mujer",
      "edad": 37,
      "nivel_estudios": "Maestria",
      "ocupacion": "Emprendedor",
      "lean_startup_general": [{
        "respuesta": 6,
        "elementos": ["Cartas de experimentos", "Tablero inicial de Ingenieria"]
      }],
      "circuito_feedback": [{
        "respuesta": 9,
        "elementos": ["Cartas de experimentos", "Tablero inicial de Ingenieria"]
      }],
      "pivotar": [{
        "respuesta": 9,
        "elementos": ["Cartas de experimentos", "Instrucciones del facilitador"]
      }],
      "propuesta_valor_canvas": [{
        "respuesta": 10,
        "elementos": ["Tablero de clientes"]
      }],
      "iteraciones_rapidas": [{
        "respuesta": 9,
        "elementos": ["Ninguno de los anteriores"]
      }],
      "producto_minimo_viable": [{
        "respuesta": 10,
        "elementos": ["Pila de características de producto", "Bloque inicial de experimentos"]
      }],
      "contabilidad_innovacion": [{
        "respuesta": 9,
        "elementos": ["Cartas de experimentos", "Hoja de contabilidad de la innovacion"]
      }],
      "perfil_cliente": [{
        "respuesta": 10,
        "elementos": ["Tablero de clientes"]
      }],
      "ajuste_problema_solucion": [{
        "respuesta": 9,
        "elementos": ["Tablero de clientes"]
      }],
    }, {
      "tipo": "despues",
      "fecha_aplicación": "2019-10-29",
      "genéro": "Hombre",
      "edad": 4037,
      "nivel_estudios": "Licenciatura",
      "ocupacion": "Emprendedor",
      "lean_startup_general": [{
        "respuesta": 10,
        "elementos": ["Tablero inicial de Ingenieria"]
      }],
      "circuito_feedback": [{
        "respuesta": 10,
        "elementos": ["Cartas de experimentos", "Tablero inicial de Ingenieria"]
      }],
      "pivotar": [{
        "respuesta": 10,
        "elementos": ["Instrucciones del facilitador", "Cartas de experimentos"]
      }],
      "propuesta_valor_canvas": [{
        "respuesta": 10,
        "elementos": ["Ninguno de los anteriores"]
      }],
      "iteraciones_rapidas": [{
        "respuesta": 9,
        "elementos": ["Ninguno de los anteriores"]
      }],
      "producto_minimo_viable": [{
        "respuesta": 10,
        "elementos": ["Cartas de experimentos", "Pila de características de producto", "Tablero inicial de Ingenieria"]
      }],
      "contabilidad_innovacion": [{
        "respuesta": 10,
        "elementos": ["Hoja de contabilidad de la innovacion"]
      }],
      "perfil_cliente": [{
        "respuesta": 10,
        "elementos": ["Tablero de clientes"]
      }],
      "ajuste_problema_solucion": [{
        "respuesta": 10,
        "elementos": ["Tablero de clientes"]
      }]
    }, {
      "tipo": "despues",
      "fecha_aplicación": "2019-10-29",
      "genéro": "Hombre",
      "edad": 34,
      "nivel_estudios": "Maestria",
      "ocupacion": "Empleado",
      "lean_startup_general": [{
        "respuesta": 10,
        "elementos": ["Cartas de experimentos", "Tablero inicial de Ingenieria"]
      }],
      "circuito_feedback": [{
        "respuesta": 9,
        "elementos": ["Cartas de experimentos", "Tablero inicial de Ingenieria"]
      }],
      "pivotar": [{
        "respuesta": 10,
        "elementos": ["Cartas de experimentos"]
      }],
      "propuesta_valor_canvas": [{
        "respuesta": 10,
        "elementos": ["Cartas de experimentos"]
      }],
      "iteraciones_rapidas": [{
        "respuesta": 10,
        "elementos": ["Ninguna de las anteriores"]
      }],
      "producto_minimo_viable": [{
        "respuesta": 10,
        "elementos": ["Cartas de experimentos", "Product backlog", "Pila de características de producto"]
      }],
      "contabilidad_innovacion": [{
        "respuesta": 9,
        "elementos": ["Hoja de contabilidad de la innovacion"]
      }],
      "perfil_cliente": [{
        "respuesta": 10,
        "elementos": ["Tablero de clientes"]
      }],
      "ajuste_problema_solucion": [{
        "respuesta": 9,
        "elementos": ["Cartas de experimentos", "Tablero de clientes"]
      }]
    }]

```

## Actualizando documentos

Si queremos hacer un update a los documentos podemos hacer consultas del tipo:

```sql

db.resultados.updateMany({
      tipo: "previo"
    }, {
      $set: {
        tipo: "antes"
      }
    })

    db.resultados.updateMany({
      ocupacion: "Independient"
    }, {
      $set: {
        tipo: "Independiente"
      }
    })

```

En la siguiente imagen se puede observar el total de documentos listos para realizar queries enfocadas en el análisis:

![ALT](/img/documents.png)

## Calculando promedio

Para realizar operaciones podemos hacer uso del [Aggregation Framework](https://docs.mongodb.com/manual/core/aggregation-pipeline/) que provee MongoDB. Las operaciones de agregación procesan registros de datos y 
devuelven resultados calculados. Las operaciones de agregación agrupan valores de varios documentos y pueden realizar una variedad de operaciones en los  datos agrupados para obtener un único resultado.

Por ejemplo, si queremos calcular el promedio de todas las respuestas del concepto de "pivotar" podemos hacerlo con el siguiente query:

Encuestas previas

```sql
db.resultados.aggregate(
      [{
        $group: {
          _id: null,
          avg_pivotar: {
            $avg: "$pivotar"
          }
        }
      }, {
        $project: {
          _id: 0
        }
      }]
    )
```
>  Podemos hacer una proyección para excluir el id del resultado utilizando $project en el último stage.

Resultado:

![ALT](/img/promedio_antes.png)


Para las encuestas posteriores sería:

```sql

db.resultados.aggregate(
      [{
        $match: {
          "tipo": "despues"
        }
      }, {
        $unwind: {
          path: "$pivotar"
        }
      }, {
        $group: {
          _id: null,
          avg_pivotar: {
            $avg: "$pivotar.respuesta"
          }
        }
      }, {
        $project: {
          _id: 0
        }
      }]
    )
```
Resultado:

![ALT](/img/promedio_despues.png)

## Calculando desviación estándar

Para la desviación estándar y otras operaciones solo hay que sustituir el acumulador con la operación que necesitamos realizar. Quedando de la siguiente
manera:

Encuestas previas

```sql
db.resultados.aggregate(
      [{
        $group: {
          _id: null,
          desviacion_estandar_pivotar: {
            $stdDevPop: "$pivotar"
          }
        }
      }, {
        $project: {
          _id: 0
        }
      }]
    )
```

>  En este caso $stdDevPop es el acumulador para calcular la desviación estándar.

Para las encuestas posteriores:

```sql

db.resultados.aggregate(
      [{
        $match: {
          "tipo": "despues"
        }
      }, {
        $unwind: {
          path: "$pivotar"
        }
      }, {
        $group: {
          _id: null,
          desviacion_estandar_pivotar: {
            $stdDevPop: "$pivotar.respuesta"
          }
        }
      }, {
        $project: {
          _id: 0
        }
      }]
    )
```


Otras operaciones que se podrían realizar son calcular el valor mínimo o máximo, entre otras.

## Analizando Schema desde MongoDB Compass

Compass permite explorar las colecciones y los documentos de manera gráfica. De tal forma que podemos conocer a detalle la cantidad total de
documentos, rangos y tipos de datos.

El reporte basado en nuestros 26 documentos nos permite ver lo siguiente

Hay un array de documentos con dos campos anidados para cada concepto:

![ALT](/img/compass1.png)

El 65 % de los encuestados son hombres.

![ALT](/img/genero.png)

La mayoría tenían 40 años de edad.

![ALT](/img/edad.png)

La mayoría tenian Maestría como máximo nivel de estudios.

![ALT](/img/nivel_estudios.png)

La mayoría, con 42 %, son empleados.

![ALT](/img/ocupacion.png)

Y por último, la mayoría tenía experiencia media en Lean Startup.

![ALT](/img/conocimiento_inicial.png)

## Conclusión


Como mencionamos antes, nuestro reporte está basado en 26 documentos. Esto quiere decir que tenemos 13 encuestas anteriores y 13 encuestas posteriores.
En general, podemos observar que hubo una tendencia a que los niveles de comprensión aumentaran. Por supuesto, hay algunos conceptos que se entendieron
mejor que otros por lo que el incremento en determinado concepto puede llegar a ser mayor en comparación con el resto.

En primer lugar se muestra el nivel de estudios de los 13 encuestados en total.

![ALT](/img/nivel_estudios_chart.png)

**7 tenían Maestría como último grado de estudios**. Indicando un alto nivel educativo en la mayoría de los encuestados, esto puede decirnos que probablemente 
la mayoría tenía un conocimiento base de los conceptos de la Metodología Lean Startup como se muestra a continuación:

![ALT](/img/conocimiento_promedio_inicial.png)

Así mismo, el resultado de aprendizaje final puede verse afectado en el sentido de no representar un aumento tan considerable debido al conocimiento inicial
de todos los encuestados.

![ALT](/img/conocimiento_promedio_final.png)

En la parte de la ocupación de los participantes, la mayoría eran empleados y solamente 2 estaban emprendiendo. Esto puede reflejar que aunque tienen el conocimiento 
base de los conceptos de Lean Startup, gran parte de ellos aún no los está poniendo en práctica para iniciar un negocio o probablemente tienen problemas para implementarlo correctamente.

![ALT](/img/ocupacion_mongo_chart.png)

El género de lo encuestados fue balanceado, teniendo 7 hombres y 6 mujeres:

![ALT](/img/genero_mongo_chart.png)

En el caso de agrupar los niveles de comprensión por género podemos obervar que al inicio las mujeres tenían un conocimiento mayor acerca de los conceptos claves en comparación
con los hombres. Sin embargo, en la encuesta final muestra como los hombres terminaron con un incremento considerable e incluso mayor que el de las mujeres en 
determinado concepto, por ejemplo, el concepto de "pivotar".

Encuesta anterior

![ALT](/img/promedio_mujer.png)

![ALT](/img/promedio_hombre.png)


Encuesta posterior

![ALT](/img/promedio_mujer_despues.png)

![ALT](/img/promedio_hombre_despues.png)


Finalmente, podemos decir que el resultado de aprendizaje puede variar dependiendo de diferentes factores como son el conocimiento inicial y la preparación de cada uno de los participantes.
Sin embargo, se obtuvieron resultados que pueden ser comparados con los talleres posteriores con edades, niveles de estudio diferentes y un conocimiento inicial en 0.Respecto a la tesis, 
es un primer avance para conocer la forma en que pueden analizarse los datos que se obtienen de las encuestas y como interpretar los resultados.

## Aprendizaje de materia de Bases de Datos NO SQL

Sin lugar a dudas, el salto de modelar bases de datos de manera relacional hacia NO SQL es muy importante. Al principio parecía algo confuso debido a que aún pensaba en seguir
modelando de forma relacional sin darme cuenta. Para esto, elegir un modelo basado en documentos no solo me ayudó a comprender como funcionan las bases de datos no sql sino también
para tratar la información con mayor naturalidad. Las encuestas son una especie de documento que contiene preguntas y respuestas, por lo tanto es así como podrían representarse en 
el modelo, dejando a un lado las relaciones y dependencias innecesarias. Además, recordar que existen otro tipo de modelados como son clave-valor, grafos, columnas, objetos, etc.

Otra cosa en particular con la que me quedó es la parte de analizar que modelo se adapta más de acuerdo al tipo de consultas que vas a realizar y sobre todo que atributos de calidad
son más importantes en nuestro desarrollo, es decir, queremos consistencia sobre disponibilidad o disponibilidad sobre tolerancia a fallos por mencionar un ejemplo. Ya que esto va a 
determinar que modelado es más efectivo y entonces concebir un modelo base. En el caso del modelo de documentos utilizado para mis encuestas me permite tener flexibilidad y consistencia
aunque sería interesante utilizar otro modelo para conocer las diferencias. Definitivamente el modelado NO SQL estará presente en mis próximos proyectos.





